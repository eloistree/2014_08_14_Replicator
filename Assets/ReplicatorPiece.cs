﻿using UnityEngine;
using System.Collections;

public class ReplicatorPiece : MonoBehaviour {

	public void Unlink(){
		this.transform.parent = null;
		if(this.gameObject.rigidbody==null)
		this.gameObject.AddComponent<Rigidbody> ();

		//this.gameObject.rigidbody.interpolation = RigidbodyInterpolation.Extrapolate;
		//this.gameObject.rigidbody.collisionDetectionMode = CollisionDetectionMode.Continuous;
		if(this.gameObject.collider==null)
		this.gameObject.AddComponent<BoxCollider> ();
	}
	
	public void LinkTo(Replicator obj){
	}
	public void LinkTo(ReplicatorPositionPiece obj){
	}
	


}

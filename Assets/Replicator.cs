﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;

public class Replicator : MonoBehaviour {

	public ReplicatorPiece[] pieces;
	public float deathPieceDuration=30f;
	public float deathPieceDurationRandom=25f;
	public bool alive;
	void Start () {
		pieces = this.gameObject.GetComponentsInChildren<ReplicatorPiece> ();
		ActivateAllPieces ();	
	}

	
	// Update is called once per frame
	void Update () {
	
	}

	public ReplicatorPiece GetRandomPiece(){
		return pieces[Random.Range(0,pieces.Length)];
	}
	public void Explosion(float force=1,float radius=1){
		if (!alive)
						return;

		float forceTest = force;
		float radiusTest = radius;
		ReplicatorPiece randomRP = GetRandomPiece (); 
		Vector3 whereTest = randomRP?randomRP.transform.position:transform.position;


		DeactivateAllPieces ();
		foreach (ReplicatorPiece rp in pieces) {
			ReplicatorExplosionPiece rep = ReplicatorExplosionPiece.Free(deathPieceDuration,deathPieceDurationRandom);
			if(rep!=null)
			{
				rep.transform.position = rp.transform.position;
				rep.transform.rotation = rp.transform.rotation;
				rep.rigidbody.AddExplosionForce(forceTest, whereTest,radiusTest);
			}
		}

	}

	void DeactivateAllPieces ()
	{
		foreach (ReplicatorPiece rp in pieces) 
			rp.gameObject.SetActive(false);
		alive = false;
	}
	void ActivateAllPieces ()
	{
		foreach (ReplicatorPiece rp in pieces) 
			rp.gameObject.SetActive(true);
		alive = true;
	}


	public void Reconstruction()
	{
		ActivateAllPieces ();
	}
}

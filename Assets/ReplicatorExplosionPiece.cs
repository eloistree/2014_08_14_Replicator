﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReplicatorExplosionPiece : MonoBehaviour {

	public static List<ReplicatorExplosionPiece> busy = new List<ReplicatorExplosionPiece>();
	public static Queue<ReplicatorExplosionPiece> available = new Queue<ReplicatorExplosionPiece>();


	void Awake()
	{
		if (this.gameObject.rigidbody == null) {
			this.gameObject.AddComponent<Rigidbody>();		
		}
		if (this.gameObject.collider == null) {
			this.gameObject.AddComponent<BoxCollider>();		
		}

		Blind ();
	}

	public static  ReplicatorExplosionPiece Free(float time, float randomTimeRange=0f)
	{
		if (available == null || available.Count <= 0) {
			if(busy==null || busy.Count<=0) return null;
			else {
				busy[0].Blind();
			}
			return null;
		}

		ReplicatorExplosionPiece tmp = available.Dequeue ();
		busy.Add (tmp);


		tmp.gameObject.SetActive (true);
		if(tmp.gameObject.rigidbody!=null)
			tmp.gameObject.rigidbody.velocity=Vector3.zero;
		tmp.UnblindIn (time+(randomTimeRange==0f?0f:Random.Range(-randomTimeRange,randomTimeRange)));
		return tmp;
	}
	private void UnblindIn(float time){
		Invoke ("Blind", time);
	}
	public void Blind()
	{
		CancelInvoke ("Blind");
		this.gameObject.SetActive (false);
		available.Enqueue (this);
		busy.Remove (this);
	


	}
}

﻿using UnityEngine;
using System.Collections;

public class ClickReplicatorExplosion : MonoBehaviour {

	public Replicator [] replicators;
	public float force=100f;
	public float radius=1f;

	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyDown (KeyCode.Space)) {
			foreach( Replicator r in replicators)
						{
							if(r.alive){r.Explosion(force,radius);}
							else r.Reconstruction();
			
						}
		}
//		if (Input.GetKeyDown (KeyCode.E)) {
//			foreach( Replicator r in replicators)
//			{
//				r.Explosion(force,radius);
//
//			}
//		}
//		if (Input.GetKeyDown (KeyCode.J)) {
//			foreach( Replicator r in replicators)
//			{
//				r.Reconstruction();
//				
//			}
//		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class CameraAutoRotation : MonoBehaviour {
	
	public float rotationPower=1f;
	public float verticalPower=1f;
	
	// Update is called once per frame
	void Update () {
		
		float horizontal=Input.GetAxis("Horizontal");
		float vertical=Input.GetAxis("Vertical");
		
		this.transform.Rotate(Vector3.up*(Time.deltaTime*rotationPower*horizontal));
		this.transform.Translate(Vector3.up*(Time.deltaTime*verticalPower*vertical));
	
	}
}

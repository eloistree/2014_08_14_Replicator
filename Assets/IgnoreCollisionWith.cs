﻿using UnityEngine;
using System.Collections;

public class IgnoreCollisionWith : MonoBehaviour {
	
	public LayerMask layerA;
	public LayerMask layerB;
	// Use this for initialization
	void Start () {
		Physics.IgnoreLayerCollision (layerA, layerB,true);
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class DelTest : MonoBehaviour {

	public Transform where;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (where == null)
						return;
		if(Input.GetKey(KeyCode.P))
		{
			ReplicatorExplosionPiece r = ReplicatorExplosionPiece.Free (5f);

			if(r!=null){
				r.gameObject.transform.position=where.position;
			}
		}
	}
}

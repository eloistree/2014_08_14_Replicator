﻿using UnityEngine;
using System.Collections;

public class AutoDuplicateAtStart : MonoBehaviour {

	public int copies=10;
	public GameObject toCopy;
	public Transform parent;
	// Use this for initialization
	void Awake () {
	
		for (int i =0; i<copies; i++) {	
			GameObject gamo =GameObject.Instantiate(toCopy,toCopy.transform.position,toCopy.transform.rotation) as GameObject;
			if(gamo!=null) gamo.transform.parent=parent;
		}
		Destroy (this);
	}

}
